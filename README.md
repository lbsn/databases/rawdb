[![version](https://lbsn.vgiscience.org/databases/rawdb/version.svg)](https://gitlab.vgiscience.de/lbsn/databases/rawdb/tree/master) 
[![pipeline status](https://gitlab.vgiscience.de/lbsn/databases/rawdb/badges/master/pipeline.svg)](https://gitlab.vgiscience.de/lbsn/databases/rawdb/commits/master)

# LBSN RAW Database - Docker Container

This is a preconfigured docker container based on the RAW version of the [common 
location based social network (LBSN) data structure concept](https://lbsn.vgiscience.org) to handle cross network 
Social Media data.

**tl:dr**

To Start the docker container locally:

```bash
git clone --recursive git@gitlab.vgiscience.de:lbsn/databases/rawdb.git
cd rawdb
mv .env.example .env
# optionally: adjust parameters in .env
docker network create lbsn-network
docker-compose up -d && docker-compose logs --follow
```

This docker container can be locally started to use the raw version of the LBSN Structure. 
If you already initialized LBSN Structure and want to upgrade to the newest version, 
have a look at the [LBSN Alembic Migrations][LBSN Alembic Migrations] repository.

A complete guide is provided in the [LBSN documentation](https://lbsn.vgiscience.org)

## Startup options

The postgres data can be created from scratch, initializing an empty structure. It 
is also possible to start the container using a backup volume (e.g. that was archived 
as a tar).

The two commands are:

* init: 
```
docker-compose -f docker-compose.yml -f docker-compose.init.yml up -d
```

This is also what happens by default, i.e. `docker-compose up -d` will init, if no 
volume is given. 

* make sure you've manually restored a backup docker volume with the name `lbsn-rawdb` 
  (e.g. check with `docker volume ls`)
* import: 
```
docker-compose -f docker-compose.yml -f docker-compose.import.yml up -d
```

## Local development

Start by cloning this repository recursively (including the init/structure submodule):


```bash
git clone --recurse-submodules git@gitlab.vgiscience.de:lbsn/databases/rawdb.git

```

To start the docker container use:

```bash
docker-compose up -d
```

To dump and recreate the database container and watch processing the init scripts:


```bash
docker-compose down && docker-compose up --build --detach && docker-compose logs 
--follow
```

[GDPR]: https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX%3A32016R0679
[data-transfer-project]: https://github.com/google/data-transfer-project
[LBSN Alembic Migrations]: https://gitlab.vgiscience.de/lbsn/theplink-docker/theplink-docker-app/tree/master/alembic
