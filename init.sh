#!/usr/bin/env bash

# exit immediately on errors
set -e

# fancy log notices
notice() {
    title="| $1 |"
    edge=$(echo "$title" | sed 's/./-/g')
    echo -e "$edge\n$title\n$edge"
}

# start processing files in ./init
init(){
    for file in /init/*; do

        case "$file" in
            *.sh)
                # is bash script executable?
                if [ -x "$file" ]; then
                    notice "executing $file"
                    "$file"
                else
                    notice "sourcing $file"
                    source "$file"
                fi
                ;;
            *.sql)
                # does the database already exist?
                if psql -lqt | cut -d \| -f 1 | grep -qw "$DATABASE_NAME"; then
                    notice "running SQL from $file on database \`$DATABASE_NAME\`"
                    # replace placeholder database name and run it on the database
                    sed -r 's/lbsn/'"$DATABASE_NAME"'/' "$file" | \
                        psql -v ON_ERROR_STOP=1 --dbname "$DATABASE_NAME"
                else
                    notice "running SQL from $file"
                    # replace placeholder database name and run it
                    sed -r 's/lbsn/'"$DATABASE_NAME"'/' "$file" | \
                        psql -v ON_ERROR_STOP=1
                fi
                ;;
            *)
                # ignore other files
                notice "ignoring $file"
                ;;
        esac
        echo

    done
}

# run init function, if not sourced with --no-exec parameter
if [ "${1}" != "--no-exec" ]; then
    init "${@}"
fi
