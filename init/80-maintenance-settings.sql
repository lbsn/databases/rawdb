/* Adjust autovacuum settings for the largest table post
 * e.g. on large post tables, auto Vacuum appears too
 * infrequently on topical.post, reduce limit
 */
ALTER TABLE topical.post SET (autovacuum_vacuum_scale_factor = 0.01);

ALTER TABLE topical.post SET (autovacuum_vacuum_threshold = 10000);


/* Optional maintenance settings
 * e.g. on large relation tables, Auto Analyze appears too often
 * on simple relations._user_connectsto_user, increase limit
 */
--ALTER TABLE relations._user_connectsto_user SET (
--  autovacuum_analyze_threshold  = 200000);
