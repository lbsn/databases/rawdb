/* Create Functions
 */
/* This function will refresh the last modified column
 * (e.g. see triggers for tables user, post, place) to the current time.
 * This information can then be used to speedy retrieval of the
 * latest new or modified entries using a partial index (e.g. last 24 hrs).
 */
CREATE OR REPLACE FUNCTION extensions.update_modified_column ()
    RETURNS TRIGGER
    AS $BODY$
BEGIN
    NEW.modified = now();
    RETURN NEW;
END;
$BODY$
LANGUAGE plpgsql
VOLATILE;


/* On Place Update -> substitute Post_latlng in post-table
 * with Place_latlng (if Geoaccuracy is Null, unknown or Place)
 * If a new coordinate is submitted for a place, posts with
 * this place that have no post_latlng will be updated with
 * place-coordinates, geoaccuracy will be set to "place".
 * In addition, city_guid will be written to posts if Null.
 * Note that post_latlng will never be NULL (these entries
 * will have Null-island geometry instead).
 */
CREATE OR REPLACE FUNCTION extensions.func_UpdatePlaceGeom ()
    RETURNS TRIGGER
    AS $BODY$
DECLARE
    null_geom text := ST_SetSRID (ST_MakePoint (0,
            0),
        4326)::text;
BEGIN
    IF NEW.geom_center IS NOT NULL AND NOT NEW.geom_center::text = null_geom AND NEW.geom_center IS DISTINCT FROM OLD.geom_center THEN
        UPDATE
            topical.post
        SET
            post_latlng = NEW.geom_center,
            post_geoaccuracy = 'place'
        WHERE (origin_id, place_guid)
        IN (
            VALUES (NEW.origin_id, NEW.place_guid))
                AND (post_geoaccuracy IS NULL
                    OR post_geoaccuracy IN ('unknown', 'place'));
    END IF;
    IF NEW.city_guid IS NOT NULL AND (NEW.city_guid IS DISTINCT FROM OLD.city_guid) THEN
        UPDATE
            topical.post
        SET
            city_guid = NEW.city_guid
        WHERE (origin_id, place_guid)
        IN (
            VALUES (NEW.origin_id, NEW.place_guid))
                AND city_guid IS NULL;
    END IF;
    RETURN NULL;
END
$BODY$
LANGUAGE plpgsql
VOLATILE
COST 100;


/* On City Update (Country ID) -> Transfer CountryID to Post table
 *  If a new Country ID is added for a city, posts with this city
 * that have no country_guid will be updated with country_guid.
 */
CREATE OR REPLACE FUNCTION extensions.func_UpdateCountryID ()
    RETURNS TRIGGER
    AS $BODY$
BEGIN
    IF NEW.country_guid IS NOT NULL AND (NEW.country_guid IS DISTINCT FROM OLD.country_guid) THEN
        UPDATE
            topical.post
        SET
            country_guid = NEW.country_guid
        WHERE (origin_id, city_guid)
        IN (
            VALUES (NEW.origin_id, NEW.city_guid))
                AND country_guid IS NULL;
    END IF;
    RETURN NULL;
END
$BODY$
LANGUAGE plpgsql
VOLATILE
COST 100;


/* On Post Update, check if LatLng is empty and substitute lower
 * granularity location information from place table
 * Also prevents overwrite of post_latlng by null island value
 */
CREATE OR REPLACE FUNCTION extensions.func_SubstitutePostLatLng_Upt ()
    RETURNS TRIGGER
    AS $BODY$
DECLARE
    db_geom_center text;
    db_city_guid text;
    db_country_guid text;
    null_geom text := ST_SetSRID (ST_MakePoint (0,
            0),
        4326)::text;
BEGIN
    IF NEW.post_latlng::text = null_geom AND OLD.post_latlng::text != null_geom THEN
        NEW.post_latlng = OLD.post_latlng;
    END IF;
    IF (OLD.post_geoaccuracy IS NULL OR OLD.post_geoaccuracy NOT IN ('place',
        'latlng')) AND (NEW.post_geoaccuracy IS NULL OR NEW.post_geoaccuracy = 'unknown') AND NEW.place_guid IS NOT NULL THEN
        SELECT
            place.geom_center,
            place.city_guid INTO db_geom_center,
            db_city_guid
        FROM
            spatial.place
        WHERE (place.origin_id, place.place_guid)
        IN (
            VALUES (NEW.origin_id, NEW.place_guid));
        IF db_geom_center IS NOT NULL AND NOT db_geom_center = null_geom THEN
            NEW.post_latlng = db_geom_center;
            NEW.post_geoaccuracy = 'place';
        END IF;
        IF db_city_guid IS NOT NULL THEN
            NEW.city_guid = db_city_guid;
            SELECT
                city.country_guid INTO db_country_guid
            FROM
                spatial.city
            WHERE (city.origin_id, city.city_guid)
            IN (
                VALUES (NEW.origin_id, db_city_guid));
            NEW.country_guid = db_country_guid;
        END IF;
    END IF;
    RETURN NEW;
END;
$BODY$
LANGUAGE plpgsql
VOLATILE
COST 100;


/* On Post Insert, check if LatLng is empty and substitute lower
 * granularity location information from place table
 * or send post to NULL Island.
 */
CREATE OR REPLACE FUNCTION extensions.func_SubstitutePostLatLng_Ins ()
    RETURNS TRIGGER
    AS $BODY$
DECLARE
    db_geom_center text;
    db_city_guid text;
    db_country_guid text;
    null_geom text := ST_SetSRID (ST_MakePoint (0,
            0),
        4326)::text;
BEGIN
    IF NEW.post_latlng IS NULL OR NEW.post_latlng::text = null_geom THEN
        IF NEW.place_guid IS NOT NULL THEN
            SELECT
                place.geom_center,
                place.city_guid INTO db_geom_center,
                db_city_guid
            FROM
                spatial.place
            WHERE (place.origin_id, place.place_guid)
            IN (
                VALUES (NEW.origin_id, NEW.place_guid));
            IF db_geom_center IS NOT NULL AND NOT db_geom_center = null_geom THEN
                NEW.post_latlng = db_geom_center;
                NEW.post_geoaccuracy = 'place';
            ELSE
                -- Add post to Null Island
                NEW.post_latlng = null_geom;
            END IF;
            IF db_city_guid IS NOT NULL THEN
                NEW.city_guid = db_city_guid;
                SELECT
                    city.country_guid INTO db_country_guid
                FROM
                    spatial.city
                WHERE (city.origin_id, city.city_guid)
                IN (
                    VALUES (NEW.origin_id, db_city_guid));
                NEW.country_guid = db_country_guid;
            END IF;
        ELSE
            -- Add post to Null Island
            NEW.post_latlng = null_geom;
        END IF;
    END IF;
    RETURN NEW;
END
$BODY$
LANGUAGE plpgsql
VOLATILE
COST 100;


/* A function for fast estimation of expensive queries
 */
CREATE OR REPLACE FUNCTION extensions.count_estimate (query text)
    RETURNS integer
    AS $$
DECLARE
    rec record;
    ROWS integer;
BEGIN
    FOR rec IN EXECUTE 'EXPLAIN ' || query LOOP
        ROWS := substring(rec. "QUERY PLAN" FROM ' rows=([[:digit:]]+)');
        EXIT
        WHEN ROWS IS NOT NULL;
    END LOOP;
    RETURN ROWS;
END;
$$
LANGUAGE plpgsql
VOLATILE STRICT;


/* A function that will return True on any whitespace or empty strings.
 */
CREATE OR REPLACE FUNCTION extensions.empty_or_whitespace (TEXT)
    RETURNS bool
    AS $$
    SELECT
        $1 ~ '^[[:space:]]*$';

$$
LANGUAGE sql
IMMUTABLE;

COMMENT ON FUNCTION extensions.empty_or_whitespace (TEXT) IS 'Find empty
 strings or strings containing only whitespace';


/* In postgres, there currently is no function to combine two arrays
 * and remove duplicates - this will do that..
 * Refers to:
 * https://gist.github.com/ryanguill/6c0e82dc7dee9d025bd27ad2abc274b9
 */
CREATE OR REPLACE FUNCTION extensions.mergeArrays (a1 ANYARRAY, a2 ANYARRAY)
    RETURNS ANYARRAY
    AS $$
    SELECT
        ARRAY_AGG(x ORDER BY x)
    FROM ( SELECT DISTINCT
            UNNEST($1 || $2) AS x) s;

$$
LANGUAGE SQL
STRICT;

/* Produce pseudonymized hash of input id with skey
 * - using skey as key
 * - sha256 cryptographic hash function
 * - encode in base64 to reduce length of hash
 * - remove trailing '=' from base64 string
 * - return as text
 */
CREATE OR REPLACE FUNCTION 
extensions.crypt_hash (id text, skey text)
RETURNS text
AS $$
    SELECT 
        RTRIM(
            ENCODE(
                HMAC(
                    id::bytea,
                    skey::bytea,
                    'sha256'), 
                'base64'),
            '=')
$$
LANGUAGE SQL
STRICT;

