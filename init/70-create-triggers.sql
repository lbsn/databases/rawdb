/* Create Triggers
 */
/* Triggers for updating the [modified]-column, indicating the
 * timestamps of the insertion or latest update for rows.
 * These triggers are only added for some of the (larger) tables,
 * to enable partial indexes (e.g. query of latest 24-hour updates).
 * See function update_modified_column()
 * Note: this will always update the modified column, no matter if
 * data has changed or not.
 * Otherwise, a new-distinct-from-old check would be needed for all
 * columns, which is quite expensive.
 * Note 2: Modtime on Insert is covered by default specification in
 * table definitions.
 */
DROP TRIGGER IF EXISTS update_user_modtime ON social.user;

CREATE TRIGGER update_user_modtime
    BEFORE UPDATE ON social.user
    FOR EACH ROW
    EXECUTE PROCEDURE extensions.update_modified_column ();

DROP TRIGGER IF EXISTS update_post_modtime ON topical.post;

CREATE TRIGGER update_post_modtime
    BEFORE UPDATE ON topical.post
    FOR EACH ROW
    EXECUTE PROCEDURE extensions.update_modified_column ();

DROP TRIGGER IF EXISTS update_place_modtime ON spatial.place;

CREATE TRIGGER update_place_modtime
    BEFORE UPDATE ON spatial.place
    FOR EACH ROW
    EXECUTE PROCEDURE extensions.update_modified_column ();

DROP TRIGGER IF EXISTS update_reaction_modtime ON topical.post_reaction;

CREATE TRIGGER update_reaction_modtime
    BEFORE UPDATE ON topical.post_reaction
    FOR EACH ROW
    EXECUTE PROCEDURE extensions.update_modified_column ();


/* Triggers extensions.func_UpdatePlaceGeom() after update.
 * In case exact spatial information is missing, this will substite
 * lower resolution spatial information for concerned rows
 * (e.g. instead of lat/lng: place or city).
 */
DROP TRIGGER IF EXISTS trig_UpdatePlaceGeom ON spatial.place;

CREATE TRIGGER trig_UpdatePlaceGeom
    AFTER UPDATE ON spatial.place
    FOR EACH ROW
    WHEN (pg_trigger_depth() < 2)
        EXECUTE PROCEDURE extensions.func_UpdatePlaceGeom ();


/* Triggers extensions.func_UpdateCountryID() after update.
 *  Will add country id to concerned posts if none has been added before.
 */
DROP TRIGGER IF EXISTS trig_UpdateCountryID ON spatial.city;

CREATE TRIGGER trig_UpdateCountryID
    AFTER UPDATE ON spatial.city
    FOR EACH ROW
    WHEN (pg_trigger_depth() < 2)
        EXECUTE PROCEDURE extensions.func_UpdateCountryID ();


/* Triggers extensions.func_SubstitutePostLatLng_Ins() after update.
 * Will update concerned posts (rows post.post_latlng and
 * post.post_geoaccuracy) with the hightest spatial information available.
 * E.g. if for some posts, only city-location was available so far and
 * place-location becomes available,
 * this will use the spatial location of the place instead and refresh
 * post.geoaccuracy to "place".
 */
DROP TRIGGER IF EXISTS trig_SubstitutePostLatLng_Ins ON topical.post;

CREATE TRIGGER trig_SubstitutePostLatLng_Ins
    BEFORE INSERT ON topical.post
    FOR EACH ROW
    WHEN (pg_trigger_depth() < 1)
        EXECUTE PROCEDURE extensions.func_SubstitutePostLatLng_Ins ();


/* Triggers extensions.func_SubstitutePostLatLng_upt() after update.
 * This is the same function as above, just that it works for updates,
 * not for new inserts.
 */
DROP TRIGGER IF EXISTS trig_SubstitutePostLatLng_Upd ON topical.post;

CREATE TRIGGER trig_SubstitutePostLatLng_Upd
    BEFORE UPDATE ON topical.post
    FOR EACH ROW
    WHEN (pg_trigger_depth() < 1)
        EXECUTE PROCEDURE extensions.func_SubstitutePostLatLng_Upt ();

