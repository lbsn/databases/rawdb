DROP SCHEMA IF EXISTS extensions CASCADE;

CREATE SCHEMA extensions;


/* Create PostGIS extension
 * PostGIS is a spatial database extender for PostgreSQL
 * object-relational database. It adds support for geographic
 * objects allowing location queries to be run in SQL.
 * https://postgis.net/
 */
CREATE EXTENSION IF NOT EXISTS postgis schema extensions;


/* Create hstore extension, a specific data type for key-value pairs
 * (e.g. used for optional additional attributes for places);
 */
CREATE EXTENSION IF NOT EXISTS hstore schema extensions;

/* Create Crypto extension
 * for Anonymization on export,
 * e.g. (user_guid = digest('salt' || 'pw','sha256')::varchar) AS "user_hash"
 * https://www.postgresql.org/docs/current/pgcrypto.html
 */
CREATE EXTENSION IF NOT EXISTS pgcrypto SCHEMA extensions;

/* Search Path for making extensions available in all schemas
 * Note explicit mentioning of DATABASE name. Further: SQL identifiers
 * that are case-sensitive need to be enclosed in double quotes.
 */
ALTER DATABASE "lbsn" SET search_path TO "$user", extensions;


/* Grant privileges to users to use
 * functions and types that will be added in the future
 */
ALTER DEFAULT PRIVILEGES IN SCHEMA extensions
   GRANT ALL PRIVILEGES ON functions TO postgres, public;
ALTER DEFAULT PRIVILEGES IN SCHEMA extensions
   GRANT ALL PRIVILEGES ON types TO postgres, public;
