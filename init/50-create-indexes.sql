/* Create Indexes
 */
CREATE INDEX gix_country_geom ON spatial.country
USING gist (geom_center);

CREATE INDEX gix_city_geom ON spatial.city
USING gist (geom_center);

CREATE INDEX gix_place_geom ON spatial.place
USING gist (geom_center);

CREATE INDEX idx_postplace ON topical.post (origin_id, place_guid);

CREATE INDEX idx_postcity ON topical.post (origin_id, city_guid);

CREATE INDEX idx_postuser ON topical.post (origin_id, user_guid);

CREATE INDEX idx_postcountry ON topical.post (origin_id, country_guid);

CREATE INDEX idx_placecity ON spatial.place (origin_id, city_guid);

CREATE INDEX idx_post_create_date ON topical.post
USING brin (post_create_date);

CREATE INDEX idx_post_publish_date ON topical.post
USING brin (post_publish_date);

CREATE INDEX gix_latlng_geom ON topical.post
USING gist (post_latlng);

CREATE INDEX idx_term ON topical.term (origin_id, term);


/* Brin indexes are pretty small, even for large tables.
 They're therefore useful for the latest modified timestamp columns,
 e.g. used for maintenance.
 */
CREATE INDEX idx_modified_post ON topical.post
USING BRIN (modified)
WHERE
    modified IS NOT NULL;

CREATE INDEX idx_modified_user ON social.user
USING BRIN (modified)
WHERE
    modified IS NOT NULL;

CREATE INDEX idx_modified_place ON spatial.place
USING BRIN (modified)
WHERE
    modified IS NOT NULL;

