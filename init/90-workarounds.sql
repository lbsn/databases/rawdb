/* Workarounds to import existing data
 * e.g. speed up import
 */
-- for migration, disable all triggers and constraints temporarily
-- to improve speed
-- SET session_replication_role to 'replica';
-- set any location with NULL location to null island (coordinates 0, 0)
-- UPDATE data.place SET geom_center = ST_SetSRID(ST_MakePoint(0, 0), 4326) WHERE geom_center is NULL;
-- enable all triggers and constraints
-- SET session_replication_role to 'origin';
