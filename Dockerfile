FROM postgres:14

# https://manpages.debian.org/stretch/debconf-doc/debconf.7.en.html#Frontends
ARG DEBIAN_FRONTEND=noninteractive

# Postgis
# https://github.com/appropriate/docker-postgis
RUN apt-get update \
&& apt-cache showpkg postgresql-14-postgis-3 \
    && apt-get install -y --no-install-recommends \
        postgresql-14-postgis-3 \
        postgresql-14-postgis-3-scripts \
        postgis \
    && rm -rf /var/lib/apt/lists/*
